# Project computational intelligence

## Links
- [Emotion Detection in Python](https://www.youtube.com/watch?v=ERXqo_ZEnIo)
- [Emotion Detection using CNN](https://www.youtube.com/watch?v=UHdrxHPRBng)
- [viso.ai deep learning](https://viso.ai/deep-learning/visual-emotion-ai-recognition/)

## used libraries:
- [deepface](https://pypi.org/project/deepface/)

## ideas:

surprise -> do nothing
disgust -> do nothing
neutral -> pick at random?
angry -> play happy music
happy -> https://v2.jokeapi.dev/joke/Any
fear -> play calm music??
sad -> play depressing music :))

music: spotify api?? the answer is yes
joke: https://v2.jokeapi.dev/joke/Any
