from spotipy.oauth2 import SpotifyOAuth
from requests import post, get
from dotenv import load_dotenv
from deepface import DeepFace
from itertools import groupby
from spotipy import Spotify
from enum import Enum

import base64
import random
import signal
import json
import cv2
import sys
import os

CONFIRM = 3

cam = cv2.VideoCapture(0)

load_dotenv()
scope = "playlist-read-private,user-modify-playback-state"

sp = Spotify(auth_manager=SpotifyOAuth(scope=scope))


class Emotion(Enum):
    SURPRISE = "surprise"
    DISGUST = "disgust"
    NEUTRAL = "neutral"
    ANGRY = "angry"
    HAPPY = "happy"
    FEAR = "fear"
    SAD = "sad"


def get_emotion() -> str | None:
    ret, img = cam.read()
    if ret:
        cv2.imencode(".jpg", img)

        try:
            results = DeepFace.analyze(img, actions=["emotion"])
        except:
            return None

        return results[0]["dominant_emotion"]
    else:
        return None


def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)


def report_emotion() -> Emotion:
    results = []
    while not len(results) == CONFIRM:
        result = get_emotion()

        if result is not None:
            if len(results) == 0 or not results[-1] == result:
                results = [result]
            else:
                results.append(result)

    return Emotion(results[0])


def list_playlists():
    results = sp.current_user_playlists()
    return results["items"]


def filtr(cb):
    return lambda playlist: playlist["owner"]["display_name"] == "Yoolayn" and cb(
        playlist
    )


def signal_handler(sig, frame):
    cam.release()
    sys.exit(0)


def play_sad():
    ft = filter(filtr(lambda x: x["name"] == "Emotion: Sad"), list_playlists())
    for x in ft:
        sp.start_playback(
            context_uri="spotify:playlist:7y9wcVX3vwAcoiJj45MW21",
            offset={"position": random.randint(1, x["tracks"]["total"])},
        )


def play_angry():
    ft = filter(filtr(lambda x: x["name"] == "Emotion: Angry"), list_playlists())
    for x in ft:
        sp.start_playback(
            context_uri="spotify:playlist:1ciUk0EUhEhV3J0Ce2d834",
            offset={"position": random.randint(1, x["tracks"]["total"])},
        )


def play_calm():
    ft = filter(filtr(lambda x: x["name"] == "Emotion: Calm"), list_playlists())
    for x in ft:
        sp.start_playback(
            context_uri="spotify:playlist:5rGTYASykWzEIlBfVtxAUY",
            offset={"position": random.randint(1, x["tracks"]["total"])},
        )


def make_joke() -> str:
    result = get("https://v2.jokeapi.dev/joke/Any")
    try:
        print("\n" + result.json()["setup"] + "\n" + result.json()["delivery"])
    except:
        print("\n" + result.json()["joke"])


def play_rand():
    r = random.randint(0, 2)
    match r:
        case 0:
            play_sad()
        case 1:
            play_angry()
        case 2:
            play_calm()


def main():
    signal.signal(signal.SIGINT, signal_handler)
    current_emotion = report_emotion()
    print(current_emotion)
    match current_emotion:
        case Emotion.ANGRY:
            play_angry()
        case Emotion.FEAR:
            play_calm()
        case Emotion.SAD:
            play_sad()
        case Emotion.HAPPY:
            make_joke()
        case Emotion.SURPRISE:
            play_calm()
            make_joke()
        case Emotion.DISGUST:
            play_rand()
            make_joke()
        case Emotion.NEUTRAL:
            play_rand()
            make_joke()


if __name__ == "__main__":
    main()
